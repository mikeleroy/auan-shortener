@extends('layouts.app')

@section('content')


    <div class="hero-head">
        <div class="hero-wrapper">


            <!--============ Page Title =========================================================================-->
            <div class="page-title">
                <div class="container">
                    <h1>Login</h1>
                </div>
                <!--end container-->
            </div>
            <!--============ End Page Title =====================================================================-->
            <div class="background"></div>
            <!--end background-->


        </div>
    </div>


    <!--*********************************************************************************************************-->
    <!--************ CONTENT ************************************************************************************-->
    <!--*********************************************************************************************************-->
    <section class="content">
        <section class="block">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <form class="form clearfix" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class="col-form-label required">Email</label>
                                <input name="email" type="email" class="form-control" id="email"
                                       placeholder="Your Email" required>
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="password" class="col-form-label required">Password</label>
                                <input name="password" type="password" class="form-control" id="password"
                                       placeholder="Password" required>

                            </div>
                            <!--end form-group-->
                            <div class="d-flex justify-content-between align-items-baseline">
                                <label>
                                    <input type="checkbox" name="remember" value="1">
                                    Remember Me
                                </label>
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </form>
                        <hr>
                        <p>
                            Forgot Your Password? <a href="{{ route('password.request') }}" class="link">Click here.</a>
                        </p>
                    </div>
                    <!--end col-md-6-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->
@endsection
