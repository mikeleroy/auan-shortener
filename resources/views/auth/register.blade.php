@extends('layouts.app')

@section('content')
    <div class="hero-head">
        <div class="hero-wrapper">


            <!--============ Page Title =========================================================================-->
            <div class="page-title">
                <div class="container">
                    <h1>Register</h1>
                </div>
                <!--end container-->
            </div>
            <!--============ End Page Title =====================================================================-->
            <div class="background"></div>
            <!--end background-->


        </div>
    </div>

    <!--*********************************************************************************************************-->
    <!--************ CONTENT ************************************************************************************-->
    <!--*********************************************************************************************************-->
    <section class="content">
        <section class="block">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">


                        <form class="form clearfix" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-form-label required">Your Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       placeholder="Your Name"
                                       required autofocus>
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="email" class="col-form-label required">Email</label>
                                <input id="email" type="email" class="form-control" name="email"
                                       placeholder="Your Email" value="{{ old('email') }}" required>
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="password" class="col-form-label required">Password</label>
                                <input id="password" type="password" class="form-control" name="password"
                                       placeholder="Password" required>
                            </div>
                            <!--end form-group-->
                            <div class="form-group">
                                <label for="password_confirmation" class="col-form-label required">Repeat Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                       placeholder="Repeat Password" required>
                            </div>
                            <!--end form-group-->
                            <div class="d-flex justify-content-between align-items-baseline">
                                <label>
                                    <input type="checkbox" name="newsletter" value="1">
                                    Accept our <a href="#" class="link">Terms &
                                        Conditions.</a>
                                </label>
                            </div>
                            <div class="form-group center">
                            <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                        </form>
                        <hr>
                        <p>
                           You are already register?  <a href="#" class="link">Click here</a>
                        </p>
                    </div>
                    <!--end col-md-6-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </section>
        <!--end block-->
    </section>
    <!--end content-->

@endsection
