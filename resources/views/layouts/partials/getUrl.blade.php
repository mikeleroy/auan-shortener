<div class="hero-url">
    <div class="hero-wrapper">

        <!--============ Page Title =========================================================================-->

        <!--============ End Page Title =====================================================================-->
        <!--============ Hero Form ==========================================================================-->
        <form class="hero-form form">
            <div class="container">
                <!--Main Form-->
                <div class="main-search-form">
                    <div class="form-row">
                        <div class="col-md-9 col-sm-9">
                            <div class="form-group">
                                <label for="what" class="col-form-label">Get your <strong>Short</strong> and <strong>Simplify</strong>
                                    url</label>
                                <input name="keyword" type="text" class="form-control" id="what"
                                       placeholder="Paste a link here...">
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-3-->
                        <div class="col-md-3 col-sm-3">
                            <button type="submit" class="btn btn-primary width-100">Generate</button>
                        </div>
                        <!--end col-md-3-->
                    </div>
                    <!--end form-row-->
                </div>
                <!--end main-search-form-->
                <!--Alternative Form-->
                <div class="alternative-search-form">
                    <a href="#collapseAlternativeSearchForm" class="icon" data-toggle="collapse" aria-expanded="false"
                       aria-controls="collapseAlternativeSearchForm"><i class="fa fa-plus"></i>More Options</a>
                    <div class="collapse" id="collapseAlternativeSearchForm">
                        <div class="wrapper">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 d-xs-grid align-items-center justify-content-between">
                                <div class="custom-text-area">
                                    Customize your <strong>Shorten</strong> link. For <strong>Free</strong>!

                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12">
                                    <div class="" style="float: right">
                                        <h2>
                                            auan.it/

                                        </h2>
                                    </div>


                                </div>
                                <!--end col-xl-6-->
                                <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <input name="custom-url" type="text" class="form-control small"
                                                       id="custom-url" placeholder="insert custom text">
                                            </div>
                                            <!--end form-group-->
                                        </div>

                                        <!--end col-md-3-->
                                    </div>

                                    <div class="check-response">
                                        <p class="icon label-warning">
                                            <i class="fa fa-warning"></i>
                                            error
                                        </p>
                                    </div>
                                    <!--end form-row-->
                                </div>
                                <!--end col-xl-6-->
                            </div>
                            <div class="custom-url-check center">

                                <a href="#" class="btn btn-success small">Check Availability</a>

                            </div>
                            <!--end row-->
                        </div>
                        <!--end wrapper-->
                    </div>
                    <!--end collapse-->
                </div>
                <!--end alternative-search-form-->
            </div>
            <!--end container-->
        </form>
        <!--============ End Hero Form ======================================================================-->

        <!--end background-->
    </div>
</div>