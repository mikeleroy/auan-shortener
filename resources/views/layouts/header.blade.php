<!-- Header
================================================== -->

<!-- Header hero -->
<header class="hero">
    <div class="hero-wrapper">
        <!--============ Secondary Navigation ===============================================================-->
        <div class="secondary-navigation">
            <div class="container">
                <ul class="left">
                    <li>
                            <span>
                                <i class="fa fa-phone"></i> +1 123 456 789
                            </span>
                    </li>
                </ul>
                <!--end left-->
                <ul class="right">
                    @if (Auth::guest())
                        <li>
                            <a href="{{ route('login') }}">
                                <i class="fa fa-sign-in"></i>Login
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('register') }}">
                                <i class="fa fa-pencil-square-o"></i>Register
                            </a>
                        </li>
                    @else
                        @if ( Auth::user())
                            <li><a href="{{ route('admin') }}">Pannello di controllo</a></li>
                        @endif
                        <li>
                            <a href="{{ route('profile') }}">
                                <i class="fa fa-user"></i>Profilo
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-off"></i>
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
                <!--end right-->
            </div>
            <!--end container-->
        </div>
        <!--============ End Secondary Navigation ===========================================================-->
        <!--============ Main Navigation ====================================================================-->
        <div class="main-navigation">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light justify-content-between">
                    <a class="navbar-brand" href="index.html">
                        <img src="{{asset('assetsui\img\logo.png')}}" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <!--Main navigation list-->
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Listing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pages</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Extras</a>

                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Contact</a>
                            </li>
                            <li class="nav-item">
                                <a href="submit.html" class="btn btn-primary text-caps btn-rounded">Submit Ad</a>
                            </li>
                        </ul>
                        <!--Main navigation list-->
                    </div>
                    <!--end navbar-collapse-->
                </nav>
                <!--end navbar-->
            </div>
            <!--end container-->
        </div>
        <!--============ End Main Navigation ================================================================-->

    </div>
    <!--end hero-wrapper-->
</header>
<!--end hero-->
