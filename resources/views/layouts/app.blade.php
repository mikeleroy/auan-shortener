<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="ThemeStarz">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.styles')


    <title>{{ config('app.name', 'Laravel') }}</title>

</head>
<body>
<div class="page home-page">

    @include('layouts.header')

    @yield('content')

    @include('layouts.footer')
</div>

@include('layouts.scripts')


</body>
</html>
