@extends('layouts.app')

@section('content')

    @include('layouts.partials.getUrl')


    <section class="content">

        <!--============ Features Steps =========================================================================-->
        <section class="block">
            <div class="container">
                <h2>Categories</h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="feature-box">
                            <figure>
                                <img src="assets/icons/feature-user.png" alt="">
                                <span>1</span>
                            </figure>
                            <h3>Create an Account</h3>
                            <p>Etiam molestie viverra dui vitae mattis. Ut velit est</p>
                        </div>
                        <!--end feature-box-->
                    </div>
                    <!--end col-->
                    <div class="col-md-3">
                        <div class="feature-box">
                            <figure>
                                <img src="assets/icons/feature-upload.png" alt="">
                                <span>2</span>
                            </figure>
                            <h3>Submit Your Ad</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <!--end feature-box-->
                    </div>
                    <!--end col-->
                    <div class="col-md-3">
                        <div class="feature-box">
                            <figure>
                                <img src="assets/icons/feature-like.png" alt="">
                                <span>3</span>
                            </figure>
                            <h3>Make a Deal</h3>
                            <p>Nunc ultrices eu urna quis cursus. Sed viverra ullamcorper</p>
                        </div>
                        <!--end feature-box-->
                    </div>
                    <!--end col-->
                    <div class="col-md-3">
                        <div class="feature-box">
                            <figure>
                                <img src="assets/icons/feature-wallet.png" alt="">
                                <span>4</span>
                            </figure>
                            <h3>Enjoy the Money!</h3>
                            <p>Integer nisl ipsum, sodales sed scelerisque nec, aliquet sit</p>
                        </div>
                        <!--end feature-box-->
                    </div>
                    <!--end col-->
                </div>
                <!--end categories-list-->
            </div>
            <!--end container-->
        </section>        <!--end block-->
        <!--============ End Features Steps =====================================================================-->
    <!--============ Newsletter =============================================================================-->
    <section class="block">
        <div class="container">
            <div class="box has-dark-background">
                <div class="row align-items-center justify-content-center d-flex">
                    <div class="col-md-10 py-5">
                        <h2>Get the Latest Ads in Your Inbox</h2>
                        <form class="form email">
                            <div class="form-row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label for="newsletter-category" class="col-form-label">Category?</label>
                                        <select name="newsletter-category" id="newsletter-category" data-placeholder="Select Category" >
                                            <option value="">Select Category</option>
                                            <option value="1">Computers</option>
                                            <option value="2">Real Estate</option>
                                            <option value="3">Cars & Motorcycles</option>
                                            <option value="4">Furniture</option>
                                            <option value="5">Pets & Animals</option>
                                        </select>
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <!--end col-md-4-->
                                <div class="col-md-7 col-sm-7">
                                    <div class="form-group">
                                        <label for="newsletter-email" class="col-form-label">Your Email</label>
                                        <input name="newsletter-email" type="email" class="form-control" id="newsletter-email" placeholder="Your Email">
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <!--end col-md-9-->
                                <div class="col-md-1 col-sm-1">
                                    <div class="form-group">
                                        <label class="invisible">.</label>
                                        <button type="submit" class="btn btn-primary width-100"><i class="fa fa-chevron-right"></i></button>
                                    </div>
                                    <!--end form-group-->
                                </div>
                                <!--end col-md-9-->
                            </div>
                        </form>
                        <!--end form-->
                    </div>
                </div>
                <div class="background">
                    <div class="background-image">
                        <img src="assets/img/hero-background-image-01.jpg" alt="">
                    </div>
                    <!--end background-image-->
                </div>
                <!--end background-->
            </div>
            <!--end box-->
        </div>
        <!--end container-->
    </section>
    <!--end block-->


</section>
<!--end content-->

@endsection