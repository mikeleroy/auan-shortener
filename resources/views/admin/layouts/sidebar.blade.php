
<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="{{route("admin.dashboard")}}">
                    <img src="{{url('/assetsui/images/logo-fifa.png')}}" width="80" alt="">
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>



        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
            <li class="active">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="entypo-gauge"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="has-sub">
                <a href="{{ route('admin.teams.index') }}">
                    <i class="entypo-down-circled"></i>
                    <span class="title">Squadre</span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('admin.teams.index') }}">
                            <span class="title">Tutti le squadre</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.teams.index') }}">
                            <span class="title">Squadre Utenti</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.teams.index') }}">
                            <span class="title">Crea nuova Squadra</span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="has-sub">
                <a href="{{ route('admin.players.index') }}">
                    <i class="entypo-users"></i>
                    <span class="title">Giocatori</span>
                </a>
                <ul>
                    <li>
                        <a href="{{ route('admin.players.index') }}">
                            <span class="title">Tutti i giocatori</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="has-sub">
                <a href="tables-main.html">
                    <i class="entypo-user"></i>
                    <span class="title">Utenti</span>
                </a>
                <ul>
                    <li>
                        <a href="{{ url('/admin/user/index') }}">
                            <span class="title">Tutti gli utenti</span>
                        </a>
                    </li>
                    <li>
                        <a href="tables-datatable.html">
                            <span class="title">other link</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="index.html" target="_blank">
                    <i class="entypo-monitor"></i>
                    <span class="title">Calendario</span>
                </a>
            </li>
            <li class="has-sub">
                <a href="ui-panels.html">
                    <i class="entypo-newspaper"></i>
                    <span class="title">Classifiche</span>
                </a>
                <ul>
                    <li>
                        <a href="ui-panels.html">
                            <span class="title">Panels</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-tiles.html">
                            <span class="title">Tiles</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-buttons.html">
                            <span class="title">Buttons</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-typography.html">
                            <span class="title">Typography</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-tabs-accordions.html">
                            <span class="title">Tabs &amp; Accordions</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-tooltips-popovers.html">
                            <span class="title">Tooltips &amp; Popovers</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-navbars.html">
                            <span class="title">Navbars</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-breadcrumbs.html">
                            <span class="title">Breadcrumbs</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-badges-labels.html">
                            <span class="title">Badges &amp; Labels</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-progress-bars.html">
                            <span class="title">Progress Bars</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-modals.html">
                            <span class="title">Modals</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-blockquotes.html">
                            <span class="title">Blockquotes</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-alerts.html">
                            <span class="title">Alerts</span>
                        </a>
                    </li>
                    <li>
                        <a href="ui-pagination.html">
                            <span class="title">Pagination</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="mailbox.html">
                    <i class="entypo-mail"></i>
                    <span class="title">Messaggi</span>
                    <span class="badge badge-secondary">8</span>
                </a>
                <ul>
                    <li>
                        <a href="mailbox.html">
                            <i class="entypo-inbox"></i>
                            <span class="title">Inbox</span>
                        </a>
                    </li>
                    <li>
                        <a href="mailbox-compose.html">
                            <i class="entypo-pencil"></i>
                            <span class="title">Compose Message</span>
                        </a>
                    </li>
                    <li>
                        <a href="mailbox-message.html">
                            <i class="entypo-attach"></i>
                            <span class="title">View Message</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="forms-main.html">
                    <i class="entypo-doc-text"></i>
                    <span class="title">News</span>
                </a>
                <ul>
                    <li>
                        <a href="forms-main.html">
                            <span class="title">Basic Elements</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-advanced.html">
                            <span class="title">Advanced Plugins</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-wizard.html">
                            <span class="title">Form Wizard</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-validation.html">
                            <span class="title">Data Validation</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-masks.html">
                            <span class="title">Input Masks</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-sliders.html">
                            <span class="title">Sliders</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-file-upload.html">
                            <span class="title">File Upload</span>
                        </a>
                    </li>
                    <li>
                        <a href="forms-wysiwyg.html">
                            <span class="title">Editors</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="entypo-gauge"></i>
                    <span class="title">Campionati</span>
                </a>
            </li>

            <li class="">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="entypo-gauge"></i>
                    <span class="title">Coppe</span>
                </a>
            </li>


        </ul>

    </div>

</div>
