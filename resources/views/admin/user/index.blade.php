@extends('admin.layouts.app')

@section('content')

    <ol class="breadcrumb bc-3">
        <li><a href="{{route("admin.users.index")}}"><i class="fa-home"></i>Home</a></li>
        <li class="active"><strong> Utenti</strong></li>
    </ol>

    <h2>Utenti</h2>
    <br>


    <div id="table-1_wrapper" class="dataTables_wrapper">


    <div class="table-head-inline">

        <div class="pull-left">
            <a href="{{route('admin.players.edit', ['code' => 0])}}"
               class="btn btn-info btn-icon icon-left left">
                Aggiungi Nuovo Utente
                <i class="entypo-user-add"></i></a>
        </div>


        <div class="pull-right">
            <div id="table-1_filter" class="dataTables_filter">
                <label>Ricerca utente:
                    <input type="search" autocomplete="off" id="search" class=""
                           placeholder="utente..."
                           aria-controls="table-1"></label>
            </div>
        </div>

    </div>

    <table class="table table-bordered responsive">
        <thead>
        <tr>
            <th></th>
            <th>Nome</th>
            <th>Campionato</th>
            <th>Squadra</th>
            <th>AZIONI</th>

        </tr>
        </thead>
        <tbody>
        @foreach ($users as $u)
            <tr>
                <td> <img src="{{ url('/uploads/avatars/'. $u->avatar )}}" width="50" alt="" class="img-circle"> </td>

                <td> {{$u->name ." ". $u->surname}}  </td>
                <td> {{$u->league}} </td>
                <td> @if( isset($u->team->user_id))
                        <div class="label label-success">{{$u->team->name}}</div>

                    @else
                        <div class="label label-secondary">Disoccupato</div>
                    @endif
                </td>
                <td><a href="{{route('admin.users.edit', ['id' => $u->id])}}"
                       class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
                        Modifica
                    </a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
                        Cancella
                    </a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
                        Scheda
                    </a>
                </td>

            </tr>
        @endforeach

        </tbody>
    </table>

    </div>
@endsection