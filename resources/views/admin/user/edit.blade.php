<?php

use Illuminate\Support\Facades\Session;

?>

@extends('admin.layouts.app')

@section('content')


    <ol class="breadcrumb bc-3">
        <li>
            <a href="{{route("admin.dashboard")}}"><i class="fa-home"></i>Home</a>
        </li>
        <li>

            <a href="{{route("admin.players.index")}}">Utenti</a>
        </li>
        <li class="active">

            <strong>Modifica Utente</strong>
        </li>
    </ol>

    <div class="title-head-page">
        @if(isset($user->name))
            <h2>Modifica Giocatore: <p class="text-danger">{{$user->name}}</p></h2>
        @else
            <h2>Crea <p class="text-danger">nuovo</p> Utente</h2>

        @endif

    </div>


    <div class="row" style="margin-top: 20px">
        <div class="col-md-6 col-sm-offset-2">
            @if($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Tutto ok!</strong> {{ $message }}
                </div>
            @elseif($message = Session::get('error'))
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Attenzione!</strong> {{ $message }}
                </div>

            @elseif(isset($message) || Session::get('') || $message==null)
                <div class="alert alert-warning"><strong>Attenzione!</strong> In questa pagina puoi modificare i dati di
                    un utente.
                </div>

            @endif

            {!! Session::forget('success') !!}

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary" data-collapsed="0">

                <div class="panel-heading">
                    <div class="panel-title">
                        Dati dell' utente
                    </div>

                </div>

                <div class="panel-body">

                    <form role="form" class="form-horizontal form-groups-bordered" id="editUser"
                          action="{{route('admin.users.update')}}"
                          method="POST" enctype="multipart/form-data">
                        {{  csrf_field() }}

                        <input name="id" type="hidden"  value="{{  isset($user->id) ? $user->id : 0 }} ">


                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Cambia Immagine</label>

                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="field-file" placeholder="Placeholder">
                                @if(isset($user->avatar))
                                    <img src="{{ url('/uploads/avatars/'. $user->avatar )}}" width="100px"  alt="" >
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nome</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="name" id="name" placeholder="inserisci nome breve"
                                       value="{{  isset($user->name) ? $user->name : '' }} ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nome</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="surname" id="surname" placeholder="inserisci nome breve"
                                       value="{{  isset($user->surname) ? $user->surname : '' }} ">
                            </div>
                        </div>





                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-success">Modifica Dari</button>
                            </div>
                            <a href="{{route("admin.users.index")}}" class="btn btn-danger">Annulla</a>

                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>
@endsection