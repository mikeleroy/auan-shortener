<?php
/**
 * Created by PhpStorm.
 * User: Leroy
 * Date: 11/02/18
 * Time: 02:44
 */

namespace fifamatch\Http\Controllers\Admin;
use fifamatch\Http\Controllers\Controller;
use fifamatch\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.user.index',['users' => $users]);
    }




    public function edit(Request $request)
    {
        if (!Auth::check() && !Auth::user()->role_id == 8) {
            abort(403, trans('messages.notAuthorised'));
        }

        if ($request->id == 0) {
            return view('admin.user.edit');

        }

        $user = User::where('id', '=', $request->id)->first();
        return view('admin.user.edit', ['user' => $user]);

    }


    public function update(Request $request)
    {


        if (!Auth::check() && !Auth::user()->role_id == 8) {
            abort(403, trans('messages.notAuthorised'));
        }


        $id = $request->input('id');
        if ($id == 0) {
            $newUser = new User();

        //    $newUser->code = rand();
            $newUser->name = $request->input('name');
            $newUser->surname = $request->input('surname');

            $newUser->save();
            Session::put('success', 'Utente inserito correttamente');

            return view('admin.user.edit', ['user' => $newUser]);

        } else {

            $user = User::where('id', '=', $request->id)->first();

            $user->name = $request->input('name');
            $user->surname = $request->input('surname');


            $user->save();
            Session::put('success', 'Utente modificato correttamente');

            return redirect()->back();

        }


    }


}
