<?php
/**
 * Created by PhpStorm.
 * User: Leroy
 * Date: 03/07/18
 * Time: 19:11
 */

namespace App\Helpers;

class Helpers
{

    public static function generateRandomCode()
    {
        $required_length = 11;
        $limit_one = rand();
        $limit_two = rand();
        $randomCode = substr(uniqid(sha1(crypt(md5(rand(min($limit_one, $limit_two), max($limit_one, $limit_two)))))), 0, $required_length);
        return $randomCode;
    }

}