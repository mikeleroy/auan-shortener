<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_manager  = Role::where('name', 'manager')->first();
        $role_admin = Role::where('name', 'admin')->first();

        $employee = new User();
        $employee->name = 'Michele';
        $employee->email = 'mike@user.it';
        $employee->password = bcrypt("mike");
        $employee->save();
        $employee->roles()->attach($role_user);

        $manager = new User();
        $manager->name = 'admin';
        $manager->email = 'admin@admin.it';
        $manager->password = bcrypt('admin');
        $manager->save();
        $manager->roles()->attach($role_admin);
    }
}
